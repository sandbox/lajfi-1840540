CONTENTS OF THIS FILE
---------------------
* Introduction
* Installation
* Configuration

INTRODUCTION
------------
Current Maintainer: Lajfi (support@halemail.eu)

HaLe Mail is an email marketing platform using state of the art technology which permits you to send emails to your different email databases. 
This 100% online tool combines a high level of performance and penetration with an enormous ease of use. Thanks to its advanced statistics, you will 
know everything about your emailing campaign, which allows you to further develop and finetune your emailing strategies no matter where you are located in the world!

The HaLe Mail 3.0 (Un)Subscribe module will allow you to create a block for each one of your "public" mailing lists. 
Each block is built up out of a "subscribe" tab containing all fields which are in that specific mailing list, and an unsubscribe tab which will allow for people to unsubscribe to that mailing list based on their email address.

INSTALLATION
------------
1. Copy the entire webform directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

CONFIGURATION
-------------
Go to the configuration page:
admin/config/services/hm-subscriptions

There you will find a list of all found public mailing lists in your HaLe Mail account.
Select the lists for which you wish to create a (un)subscribe block.

Now go to /admin/structure/blocks and enable the block(s) that was (were) created for you.
You may want to adapt the styling of the block off course.
