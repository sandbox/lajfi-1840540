<?php

/**
 * @file
 * Theme the (Un)Subscribe block
 */
?>
<div class="hm-block-tabs">
  <ul>
    <li><a href="#subscribe-<?php print $list_id ?>"><?php print t('Subscribe', array(), array('context' => 'hm-combi-block-tab-title')) ?></a></li>
    <li><a href="#unsubscribe-<?php print $list_id ?>"><?php print t('Unsubscribe', array(), array('context' => 'hm-combi-block-tab-title')) ?></a></li>
  </ul>
  <div id="subscribe-<?php print $list_id ?>">
    <span class="hm-block-introduction"><?php print $introduction_html ?></span>
    <?php print $subscribe_form_html ?>
  </div>
  <div id="unsubscribe-<?php print $list_id ?>"><?php print $unsubscribe_form_html ?></div>
</div>
<?php
//  load jQuery UI Tabs library
drupal_add_library('system', 'ui.tabs');
//  initiate jQuery UI Tabs 
drupal_add_js('
Drupal.behaviors.hm_subscriptions_combi_block = Drupal.behaviors.hm_subscriptions_combi_block || {};
Drupal.behaviors.hm_subscriptions_combi_block.attach = function(context) {
  jQuery(".hm-block-tabs", context).tabs();
}
', 'inline');
?>